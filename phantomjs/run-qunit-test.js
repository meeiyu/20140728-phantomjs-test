/**
 * Wait until the test condition is true or a timeout occurs. Useful for waiting
 * on a server response or for a ui change (fadeIn, etc.) to occur.
 *
 * @param testFx javascript condition that evaluates to a boolean,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param onReady what to do when testFx condition is fulfilled,
 * it can be passed in as a string (e.g.: "1 == 1" or "$('#bar').is(':visible')" or
 * as a callback function.
 * @param timeOutMillis the max amount of time to wait. If not specified, 3 sec is used.
 */

var fs    = require('fs');
var page  = require('webpage').create();

function waitFor_qunit_testresult(testFx, onReady, timeOutMillis){
    var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 30001, //< Default Max Timout is 3s
        start = new Date().getTime(),
        condition = false,
        interval = setInterval(function() {
            if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
                // If not time-out yet and condition not yet fulfilled
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
            } else {
                if(!condition) {
                    // If condition still not fulfilled (timeout but condition is 'false')
                    console.log("'waitFor_qunit_testresult()' timeout\n");
                    phantom.exit(1);
                } else {
                    // Condition fulfilled (timeout and/or condition is 'true')
                    console.log("'waitFor_qunit_testresult()' finished in " + (new Date().getTime() - start) + "ms.\n");
                    typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                    clearInterval(interval); //< Stop this interval
                }
            }
        }, 100); //< repeat check every 250ms
};


function test_phantomjs(){
    var CurrentLanguage;
    console.log("test_phantomjs \n");
    
    page.includeJs('jquery-1.11.0.min.js');
    page.evaluate(function() {
        $('#select_Language option[value=zh-TW]').prop('selected', true);
        CurrentLanguage = $("#select_Language").val();
        console.log("language is " + CurrentLanguage);
    });
    console.log("Run test_phantomjs() Done");
}

if (phantom.args.length === 0 || phantom.args.length > 2){
    console.log('Usage: run-qunit.js URL\n');
    phantom.exit(1);
}

// Route "console.log()" calls from within the Page context to the main Phantom context (i.e. current "this")
page.onConsoleMessage = function(msg) {
    console.log(msg + "\n");
};

page.open(phantom.args[0], function(status){

    console.log("phantomJS run page open : " + status);
 
    if (status !== "success") {
        console.log("Unable to access network.");
        console.error('Failed to open web page.');
        phantom.exit(1);
    } else {
        console.log("-------------------------- phantomjs Test Result ----------------------------");
        test_phantomjs();
        console.log("-------------------------- phantomjs Test Done   ----------------------------");
        console.log("\n");       
        waitFor_qunit_testresult(function(){
            return page.evaluate(function(){
                var el = document.getElementById('qunit-testresult');
                if (el && el.innerText.match('completed')) {
                    console.log("--------------------------- Qunit Test Result ------------------------------");
                    return true;
                }
                return false;
            });
        }, function(){
            var failedNum = page.evaluate(function(){
                var el = document.getElementById('qunit-testresult');
                console.log(el.innerText);
                console.log("--------------------------- Done!!!!!!!!!!!!! -------------------------------");
                try {
                    return el.getElementsByClassName('failed')[0].innerHTML;
                } catch (e) { }
                return 10000;
            });
            phantom.exit((parseInt(failedNum, 10) > 0) ? 1 : 0);
        });
    }
});